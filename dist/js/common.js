/** Изоляция */
var mobile=(/iphone|iemobile|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));

(function($) {

	var header = $(".header"),
		headerSlick = $(".header__slick"),
		headerHeight = header.outerHeight(true);

	$(window).on('scroll', function() {
		if( $(window).scrollTop() > 0 ){
			header.addClass('inverse');
		} else {
			header.removeClass('inverse');
		}
	});

	// $('.header .navbar-toggle').on('click', function () {
	// 	if ( $(this).not('.collapsed') ){
	// 		header.addClass('open');
	// 	} else if ( $(this).is('.collapsed') ){
	// 		header.removeClass('open');
	// 		console.log('done');
	// 	}
	// });
	$('#main-menu').on('show.bs.collapse', function () {
		header.addClass('open');
	}).on('hide.bs.collapse', function () {
		header.removeClass('open');
	});

	headerSlick.css({
		'height' : headerHeight
	});

	header.next().css({
		'padding-top' : headerHeight
	});

	$(window).on('resize', function () {
		headerSlick.css({'height' : headerHeight});
		header.next().css({'padding-top' : headerHeight});
	});

	function postsCarousel() {
		var checkWidth = $(window).width();
		var owlPost = $(".why__icons");
		if (checkWidth >= 768) {
			if(typeof owlPost.data('owl.carousel') != 'undefined'){
				owlPost.data('owl.carousel').destroy();
			}
			owlPost.removeClass('owl-carousel');
		} else if (checkWidth < 768) {
			owlPost.addClass('owl-carousel');
			owlPost.owlCarousel({
				items : 1,
				nav : true,
				navText : ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
				// slideSpeed : 500,
				// animateOut: 'fadeOut',
				// touchDrag: false,
				// mouseDrag: false,
				autoplay: false,
				// autoplaySpeed: 8000,
				// autoplayTimeout: 8000,
				dots: true,
				loop: true
			});
		}
	}

	postsCarousel();
	$(window).resize(postsCarousel);

	$('[data-id]').scrollie({
		scrollOffset : 150,
		scrollingOutOfView : function (elem) {
			var idBlock = elem.data('id');
			$('.bg__data').each(function () {
				$(this).css({opacity: 0});
				$('.bg__data_' + idBlock).css({opacity: 1});
			});
		}
	});

})(jQuery);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21tb24uanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqINCY0LfQvtC70Y/RhtC40Y8gKi9cclxudmFyIG1vYmlsZT0oL2lwaG9uZXxpZW1vYmlsZXxpcGFkfGlwb2R8YW5kcm9pZHxibGFja2JlcnJ5fG1pbml8d2luZG93c1xcc2NlfHBhbG0vaS50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKSkpO1xyXG5cclxuKGZ1bmN0aW9uKCQpIHtcclxuXHJcblx0dmFyIGhlYWRlciA9ICQoXCIuaGVhZGVyXCIpLFxyXG5cdFx0aGVhZGVyU2xpY2sgPSAkKFwiLmhlYWRlcl9fc2xpY2tcIiksXHJcblx0XHRoZWFkZXJIZWlnaHQgPSBoZWFkZXIub3V0ZXJIZWlnaHQodHJ1ZSk7XHJcblxyXG5cdCQod2luZG93KS5vbignc2Nyb2xsJywgZnVuY3Rpb24oKSB7XHJcblx0XHRpZiggJCh3aW5kb3cpLnNjcm9sbFRvcCgpID4gMCApe1xyXG5cdFx0XHRoZWFkZXIuYWRkQ2xhc3MoJ2ludmVyc2UnKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdGhlYWRlci5yZW1vdmVDbGFzcygnaW52ZXJzZScpO1xyXG5cdFx0fVxyXG5cdH0pO1xyXG5cclxuXHQvLyAkKCcuaGVhZGVyIC5uYXZiYXItdG9nZ2xlJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG5cdC8vIFx0aWYgKCAkKHRoaXMpLm5vdCgnLmNvbGxhcHNlZCcpICl7XHJcblx0Ly8gXHRcdGhlYWRlci5hZGRDbGFzcygnb3BlbicpO1xyXG5cdC8vIFx0fSBlbHNlIGlmICggJCh0aGlzKS5pcygnLmNvbGxhcHNlZCcpICl7XHJcblx0Ly8gXHRcdGhlYWRlci5yZW1vdmVDbGFzcygnb3BlbicpO1xyXG5cdC8vIFx0XHRjb25zb2xlLmxvZygnZG9uZScpO1xyXG5cdC8vIFx0fVxyXG5cdC8vIH0pO1xyXG5cdCQoJyNtYWluLW1lbnUnKS5vbignc2hvdy5icy5jb2xsYXBzZScsIGZ1bmN0aW9uICgpIHtcclxuXHRcdGhlYWRlci5hZGRDbGFzcygnb3BlbicpO1xyXG5cdH0pLm9uKCdoaWRlLmJzLmNvbGxhcHNlJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0aGVhZGVyLnJlbW92ZUNsYXNzKCdvcGVuJyk7XHJcblx0fSk7XHJcblxyXG5cdGhlYWRlclNsaWNrLmNzcyh7XHJcblx0XHQnaGVpZ2h0JyA6IGhlYWRlckhlaWdodFxyXG5cdH0pO1xyXG5cclxuXHRoZWFkZXIubmV4dCgpLmNzcyh7XHJcblx0XHQncGFkZGluZy10b3AnIDogaGVhZGVySGVpZ2h0XHJcblx0fSk7XHJcblxyXG5cdCQod2luZG93KS5vbigncmVzaXplJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0aGVhZGVyU2xpY2suY3NzKHsnaGVpZ2h0JyA6IGhlYWRlckhlaWdodH0pO1xyXG5cdFx0aGVhZGVyLm5leHQoKS5jc3MoeydwYWRkaW5nLXRvcCcgOiBoZWFkZXJIZWlnaHR9KTtcclxuXHR9KTtcclxuXHJcblx0ZnVuY3Rpb24gcG9zdHNDYXJvdXNlbCgpIHtcclxuXHRcdHZhciBjaGVja1dpZHRoID0gJCh3aW5kb3cpLndpZHRoKCk7XHJcblx0XHR2YXIgb3dsUG9zdCA9ICQoXCIud2h5X19pY29uc1wiKTtcclxuXHRcdGlmIChjaGVja1dpZHRoID49IDc2OCkge1xyXG5cdFx0XHRpZih0eXBlb2Ygb3dsUG9zdC5kYXRhKCdvd2wuY2Fyb3VzZWwnKSAhPSAndW5kZWZpbmVkJyl7XHJcblx0XHRcdFx0b3dsUG9zdC5kYXRhKCdvd2wuY2Fyb3VzZWwnKS5kZXN0cm95KCk7XHJcblx0XHRcdH1cclxuXHRcdFx0b3dsUG9zdC5yZW1vdmVDbGFzcygnb3dsLWNhcm91c2VsJyk7XHJcblx0XHR9IGVsc2UgaWYgKGNoZWNrV2lkdGggPCA3NjgpIHtcclxuXHRcdFx0b3dsUG9zdC5hZGRDbGFzcygnb3dsLWNhcm91c2VsJyk7XHJcblx0XHRcdG93bFBvc3Qub3dsQ2Fyb3VzZWwoe1xyXG5cdFx0XHRcdGl0ZW1zIDogMSxcclxuXHRcdFx0XHRuYXYgOiB0cnVlLFxyXG5cdFx0XHRcdG5hdlRleHQgOiBbJzxpIGNsYXNzPVwiZmFzIGZhLWFuZ2xlLWxlZnRcIj48L2k+JywgJzxpIGNsYXNzPVwiZmFzIGZhLWFuZ2xlLXJpZ2h0XCI+PC9pPiddLFxyXG5cdFx0XHRcdC8vIHNsaWRlU3BlZWQgOiA1MDAsXHJcblx0XHRcdFx0Ly8gYW5pbWF0ZU91dDogJ2ZhZGVPdXQnLFxyXG5cdFx0XHRcdC8vIHRvdWNoRHJhZzogZmFsc2UsXHJcblx0XHRcdFx0Ly8gbW91c2VEcmFnOiBmYWxzZSxcclxuXHRcdFx0XHRhdXRvcGxheTogZmFsc2UsXHJcblx0XHRcdFx0Ly8gYXV0b3BsYXlTcGVlZDogODAwMCxcclxuXHRcdFx0XHQvLyBhdXRvcGxheVRpbWVvdXQ6IDgwMDAsXHJcblx0XHRcdFx0ZG90czogdHJ1ZSxcclxuXHRcdFx0XHRsb29wOiB0cnVlXHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cG9zdHNDYXJvdXNlbCgpO1xyXG5cdCQod2luZG93KS5yZXNpemUocG9zdHNDYXJvdXNlbCk7XHJcblxyXG5cdCQoJ1tkYXRhLWlkXScpLnNjcm9sbGllKHtcclxuXHRcdHNjcm9sbE9mZnNldCA6IDE1MCxcclxuXHRcdHNjcm9sbGluZ091dE9mVmlldyA6IGZ1bmN0aW9uIChlbGVtKSB7XHJcblx0XHRcdHZhciBpZEJsb2NrID0gZWxlbS5kYXRhKCdpZCcpO1xyXG5cdFx0XHQkKCcuYmdfX2RhdGEnKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHQkKHRoaXMpLmNzcyh7b3BhY2l0eTogMH0pO1xyXG5cdFx0XHRcdCQoJy5iZ19fZGF0YV8nICsgaWRCbG9jaykuY3NzKHtvcGFjaXR5OiAxfSk7XHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdH0pO1xyXG5cclxufSkoalF1ZXJ5KTsiXSwiZmlsZSI6ImNvbW1vbi5qcyJ9
