/** Изоляция */
var mobile=(/iphone|iemobile|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));

(function($) {

	var header = $(".header"),
		headerSlick = $(".header__slick"),
		headerHeight = header.outerHeight(true);

	$(window).on('scroll', function() {
		if( $(window).scrollTop() > 0 ){
			header.addClass('inverse');
		} else {
			header.removeClass('inverse');
		}
	});

	// $('.header .navbar-toggle').on('click', function () {
	// 	if ( $(this).not('.collapsed') ){
	// 		header.addClass('open');
	// 	} else if ( $(this).is('.collapsed') ){
	// 		header.removeClass('open');
	// 		console.log('done');
	// 	}
	// });
	$('#main-menu').on('show.bs.collapse', function () {
		header.addClass('open');
	}).on('hide.bs.collapse', function () {
		header.removeClass('open');
	});

	headerSlick.css({
		'height' : headerHeight
	});

	header.next().css({
		'padding-top' : headerHeight
	});

	$(window).on('resize', function () {
		headerSlick.css({'height' : headerHeight});
		header.next().css({'padding-top' : headerHeight});
	});

	function postsCarousel() {
		var checkWidth = $(window).width();
		var owlPost = $(".why__icons");
		if (checkWidth >= 768) {
			if(typeof owlPost.data('owl.carousel') != 'undefined'){
				owlPost.data('owl.carousel').destroy();
			}
			owlPost.removeClass('owl-carousel');
		} else if (checkWidth < 768) {
			owlPost.addClass('owl-carousel');
			owlPost.owlCarousel({
				items : 1,
				nav : true,
				navText : ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
				// slideSpeed : 500,
				// animateOut: 'fadeOut',
				// touchDrag: false,
				// mouseDrag: false,
				autoplay: false,
				// autoplaySpeed: 8000,
				// autoplayTimeout: 8000,
				dots: true,
				loop: true
			});
		}
	}

	postsCarousel();
	$(window).resize(postsCarousel);

	$('[data-id]').scrollie({
		scrollOffset : 150,
		scrollingOutOfView : function (elem) {
			var idBlock = elem.data('id');
			$('.bg__data').each(function () {
				$(this).css({opacity: 0});
				$('.bg__data_' + idBlock).css({opacity: 1});
			});
		}
	});

})(jQuery);