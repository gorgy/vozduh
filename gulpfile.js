'use strict;'

var gulp = require('gulp'), //основной плагин gulp
    stylus = require('gulp-stylus'), //препроцессор stylus
    prefixer = require('gulp-autoprefixer'), //расставление автопрефиксов
    cssmin = require('gulp-minify-css'), //минификация css
    concat = require('gulp-concat'), //concat js
    uglify = require('gulp-uglify'), //минификация js
    jshint = require("gulp-jshint"), //отслеживание ошибкок в js
    imagemin = require('gulp-imagemin'), //минимизация изображений
    sourcemaps = require('gulp-sourcemaps'), //sourcemaps
    rename = require("gulp-rename"), //переименвоание файлов
    plumber = require("gulp-plumber"), //предохранитель для остановки гальпа
    nib = require('nib'),
    rigger = require('gulp-rigger'),
    pug = require('gulp-pug'),
    browserSync = require('browser-sync'),
    watch = require('gulp-watch'); //расширение возможностей watch

var config = {
    server: {
        baseDir: "./dist"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "Frontend_Devil",
    codeSync: false
};

var path = {

    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'dist/',
        js: 'dist/js/',
        css: 'dist/css/',
        img: 'dist/img/',
        fonts: 'dist/fonts/',
    },
    app: { //Пути откуда брать исходники
        js: 'app/js/[^_]*.js',//В стилях и скриптах нам понадобятся только main файлы
        jshint: 'app/js/*.js',
        css: 'app/stylus/[^_]*.styl',
        img: 'app/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'app/fonts/**/*.*',
        html: 'app/[^_]*.pug'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        js: 'app/js/**/*.js',
        css: 'app/stylus/**/*.*',
        img: 'app/img/**/*.*',
        fonts: 'app/fonts/**/*.*',
        html: 'app/**/*.pug'
    },
	min:{
    	js: ['dist/js/*.js', '!dist/js/*.min.js'],
		css: ['dist/css/*.css', '!dist/css/*.min.css']
	}
};

// проверка js на ошибки и вывод их в консоль
gulp.task('jshint:build', function() {
    return gulp.src(path.app.jshint) //выберем файлы по нужному пути
        .pipe(jshint()) //прогоним через jshint
        .pipe(jshint.reporter('jshint-stylish')); //стилизуем вывод ошибок в консоль
});

// билдинг яваскрипта
gulp.task('jq:copy', function () {
	gulp.src([
		"node_modules/jquery/dist/jquery.min.js"
	])
		.pipe(gulp.dest(path.build.js))
});

gulp.task('js:build', function () {
	console.log("Start JS Build");
    gulp.src(path.app.js) //Найдем наш main файл
		.pipe(plumber())
        .pipe(rigger()) //Прогоним через rigger
        .pipe(sourcemaps.init()) //Инициализируем sourcemap
        .pipe(sourcemaps.write()) //Пропишем карты
        .pipe(gulp.dest(path.build.js)) //выгрузим готовый файл в build
        // .pipe(connect.reload()) //И перезагрузим сервер
});
gulp.task('js:min', ['js:build'], function () {
	console.log("Start JS MIN");
	gulp.src(path.min.js)
		.pipe(rename({suffix: '.min'})) //добавим суффикс .min к выходному файлу
		.pipe(uglify()) //Сожмем наш js
		.pipe(gulp.dest(path.build.js)) //выгрузим готовый файл в build
});

// билдим статичные изображения
gulp.task('image:build', function () {
    gulp.src(path.app.img) //Выберем наши картинки
        .pipe(imagemin({ //Сожмем их
            progressive: true, //сжатие .jpg
            svgoPlugins: [{removeViewBox: false}], //сжатие .svg
            interlaced: true, //сжатие .gif
            optimizationLevel: 4 //степень сжатия от 0 до 7
        }))
        .pipe(gulp.dest(path.build.img)) //выгрузим в build
        // .pipe(connect.reload()) //перезагрузим сервер
});

gulp.task('html:build', function () {
	gulp.src(path.app.html)
		.pipe(plumber())
		.pipe(pug({
			'pretty': '\t'
		}))
		.pipe(gulp.dest(path.build.html))
});

// билдинг пользовательского css
gulp.task('css:build', function () {
	console.log("Start CSS Build");
    gulp.src(path.app.css) //Выберем наш основной файл стилей
        // .pipe(sourcemaps.init()) //инициализируем soucemap
        .pipe(plumber())
		.pipe(sourcemaps.init()) //Инициализируем sourcemap
        .pipe(stylus({
            use: nib(),
            // compress: true,
            'include css': true
        })) //Скомпилируем stylus
        .pipe(prefixer({
            browsers: ['last 3 version', "> 1%", "ie 8", "ie 7"]
        })) //Добавим вендорные префиксы
		.pipe(sourcemaps.write()) //пропишем sourcemap
		.pipe(gulp.dest(path.build.css)) //вызгрузим в build
        // .pipe(connect.reload()) //перезагрузим сервер
});
gulp.task('css:min', ['css:build'], function () {
	console.log("Start CSS MIN");
    gulp.src(path.min.css) //Выберем наш основной файл стилей
		.pipe(cssmin()) //Сожмем
		.pipe(rename({suffix: '.min'})) //добавим суффикс .min к имени выходного файла
		.pipe(gulp.dest(path.build.css)) //вызгрузим в build
});

// билдим шрифты
gulp.task('fonts:build', function() {
    gulp.src(path.app.fonts)
        .pipe(gulp.dest(path.build.fonts)) //выгружаем в build
});
gulp.task('min', ['js:min','css:min']);
// билдим все
gulp.task('build', [
    'html:build',
    'jq:copy',
    'jshint:build',
    'js:build',
    // 'sprites:build',
    'css:build',
    'fonts:build',
    // 'htaccess:build',
    'image:build',
    // 'imagescontent:build'
]);

// watch
gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.css], function(event, cb) {
        gulp.start('css:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

// Server
gulp.task('webserver', function () {
    browserSync(config);
});

// действия по умолчанию
gulp.task('default', ['build', 'webserver', 'watch']);